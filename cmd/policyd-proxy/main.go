package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"

	"git.autistici.org/ai3/go-common/unix"
	"github.com/coreos/go-systemd/v22/daemon"
	"gopkg.in/yaml.v3"

	"git.autistici.org/ai3/tools/postfix-policyd-proxy"
)

var (
	configPath              = flag.String("config", "/etc/postfix-policyd-proxy.yml", "configuration file `path`")
	socketPath              = flag.String("socket", "/run/policyd-proxy/socket", "`path` to the UNIX socket to listen on")
	systemdSocketActivation = flag.Bool("systemd-socket", false, "use SystemD socket activation")
)

type config struct {
	LDAP struct {
		URI       string `yaml:"uri"`
		BindDN    string `yaml:"bind_dn"`
		BindPW    string `yaml:"bind_pw"`
		BaseDN    string `yaml:"base_dn"`
		Filter    string `yaml:"filter"`
		Attr      string `yaml:"attr"`
		ResultFmt string `yaml:"result_fmt"`
	} `yaml:"ldap"`

	NotFoundAction string `yaml:"not_found_action"`
}

func loadConfig(path string) (*config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := config{
		NotFoundAction: "DUNNO",
	}
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig(*configPath)
	if err != nil {
		log.Fatal("could not load configuration: %v", err)
	}

	d, err := policydproxy.NewLDAPDirector(
		config.LDAP.URI,
		config.LDAP.BindDN,
		config.LDAP.BindPW,
		config.LDAP.BaseDN,
		config.LDAP.Filter,
		config.LDAP.Attr,
		config.LDAP.ResultFmt,
	)
	if err != nil {
		log.Fatal("error with LDAP configuration: %v", err)
	}

	policySrv := policydproxy.NewProxy(d, config.NotFoundAction)

	var sockSrv *unix.SocketServer
	if *systemdSocketActivation {
		sockSrv, err = unix.NewSystemdSocketServer(policySrv)
	} else {
		sockSrv, err = unix.NewUNIXSocketServer(*socketPath, policySrv)
	}
	if err != nil {
		log.Fatalf("error creating server: %v", err)
	}

	done := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		sockSrv.Close()
		close(done)
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting")
	daemon.SdNotify(false, "READY=1")
	if err := sockSrv.Serve(); err != nil {
		log.Fatal(err)
	}

	<-done
}
