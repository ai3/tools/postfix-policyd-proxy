package policydproxy

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"time"
)

var requestTimeout = 5 * time.Second

// A Director provides us with an upstream address (in host:port
// form), given a recipient email address.
type Director interface {
	Lookup(context.Context, string) (string, error)
}

// Proxy implements the policyd-proxy service as a go-common/unix
// Handler. It understands the Postfix policy service protocol as
// documented on http://www.postfix.org/SMTPD_POLICY_README.html.
type Proxy struct {
	director       Director
	notFoundAction string
}

// NewProxy creates a new Proxy with the specified Director.
func NewProxy(director Director, notFoundAction string) *Proxy {
	return &Proxy{
		director:       director,
		notFoundAction: notFoundAction,
	}
}

// ServeConnection implements the Handler interface.
func (s *Proxy) ServeConnection(c net.Conn) {
	peer := c.RemoteAddr().String()

	// Run until the connection is closed or we're told to
	// shutdown for some reason.
	r := bufio.NewReader(c)
	var inRequest bool
	var req *request
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			log.Printf("%s: %v", peer, err)
			return
		}
		switch {
		case line == "\n" && inRequest:
			ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
			resp, err := s.handle(ctx, req)
			cancel()
			if err != nil {
				log.Printf("%s: error handling request: %v", peer, err)
				return
			}
			if _, err := c.Write(resp); err != nil {
				log.Printf("%s: write error: %v", peer, err)
				return
			}
			req = nil
			inRequest = false
		case line != "\n":
			if !inRequest {
				req = newRequest()
				inRequest = true
			}
			if err := req.parseLine(line); err != nil {
				log.Printf("%s: error: %v", peer, err)
				return
			}
		default:
			log.Printf("%s: protocol error", peer)
			return
		}
	}
}

func (s *Proxy) handle(ctx context.Context, req *request) ([]byte, error) {
	addr, err := s.director.Lookup(ctx, req.recipient)
	if err != nil || addr == "" {
		// No such user (or lookup error).
		return []byte(fmt.Sprintf("action=%s\n\n", s.notFoundAction)), nil
	}

	log.Printf("%s -> %s", req.recipient, addr)

	return makeRequest(ctx, addr, req)
}

// The incoming request is not actually parsed, individual records are
// just collected so that they can be replayed verbatim to the
// upstream. Only the recipient is extracted from the incoming
// request.
type request struct {
	recipient string
	values    []string
}

func newRequest() *request {
	return &request{}
}

func (r *request) parseLine(line string) error {
	if strings.HasPrefix(line, "recipient=") {
		r.recipient = line[10 : len(line)-1]
	}
	r.values = append(r.values, line)
	return nil
}

func (r *request) writeTo(w io.Writer) error {
	b := bufio.NewWriter(w)
	for _, line := range r.values {
		_, err := b.WriteString(line)
		if err != nil {
			return err
		}
	}
	_, err := b.WriteString("\n")
	if err != nil {
		return err
	}
	return b.Flush()
}

var dialer = new(net.Dialer)

func makeRequest(ctx context.Context, addr string, req *request) ([]byte, error) {
	c, err := dialer.DialContext(ctx, "tcp", addr)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	if err := req.writeTo(c); err != nil {
		return nil, err
	}

	// Read the response, and the empty line that follows it.
	r := bufio.NewReader(c)
	var resp []byte
	for i := 0; i < 2; i++ {
		line, err := r.ReadSlice('\n')
		if err != nil {
			return nil, err
		}
		resp = append(resp, line...)
	}
	return resp, nil
}
