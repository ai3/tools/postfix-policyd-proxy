module git.autistici.org/ai3/tools/postfix-policyd-proxy

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/go-ldap/ldap/v3 v3.2.4
	gopkg.in/yaml.v3 v3.0.1
)
