postfix-policyd-proxy
===

A proxy for TCP-based Postfix policy services, that knows where to
route the actual request based on the result of an LDAP query for the
recipient.

The reason for the existence of this tool is that we don't know how to
tell Postfix to combine a LDAP map as the check_policy_service
backend.

# Usage

The proxying behavior is controlled by a configuration file, in YAML
format, which supports the following attributes:

* `not_found_action` - (default: DUNNO) action to return to Postfix
  when the user is not found in the database
* `ldap`
  * `uri` - connection string
  * `bind_dn` - bind DN, for authentication
  * `bind_pw` - bind password
  * `base_dn` - base DN for the lookup query
  * `filter` - filter for the lookup query
  * `attr` - result attribute for the lookup query
  * `result_fmt` - result format string to obtain the upstream
    host:port address, where the literal token `%s` is replaced by the
    value of the *attr* attribute

The LDAP parameters are pretty much all mandatory.
