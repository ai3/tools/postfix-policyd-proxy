# git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
## explicit
git.autistici.org/ai3/go-common/ldap
git.autistici.org/ai3/go-common/unix
# github.com/Azure/go-ntlmssp v0.0.0-20200615164410-66371956d46c
github.com/Azure/go-ntlmssp
# github.com/beorn7/perks v1.0.1
github.com/beorn7/perks/quantile
# github.com/cenkalti/backoff/v4 v4.1.0
github.com/cenkalti/backoff/v4
# github.com/cespare/xxhash/v2 v2.1.1
github.com/cespare/xxhash/v2
# github.com/coreos/go-systemd/v22 v22.5.0
## explicit
github.com/coreos/go-systemd/v22/activation
github.com/coreos/go-systemd/v22/daemon
# github.com/go-asn1-ber/asn1-ber v1.5.3
github.com/go-asn1-ber/asn1-ber
# github.com/go-ldap/ldap/v3 v3.2.4
## explicit
github.com/go-ldap/ldap/v3
# github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
github.com/golang/groupcache/lru
# github.com/golang/protobuf v1.4.3
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/matttproud/golang_protobuf_extensions v1.0.1
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/prometheus/client_golang v1.9.0
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/internal
# github.com/prometheus/client_model v0.2.0
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.15.0
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.2.0
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/theckman/go-flock v0.8.0
github.com/theckman/go-flock
# go.opencensus.io v0.22.5
go.opencensus.io
go.opencensus.io/internal
go.opencensus.io/trace
go.opencensus.io/trace/internal
go.opencensus.io/trace/tracestate
# golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
golang.org/x/crypto/md4
# golang.org/x/sys v0.0.0-20201214210602-f9fddec55a1e
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# google.golang.org/protobuf v1.25.0
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/fieldsort
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/mapsort
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
