package policydproxy

import (
	"context"
	"errors"
	"strings"

	"git.autistici.org/ai3/go-common/ldap"
	"github.com/go-ldap/ldap/v3"
)

const poolSize = 3

type ldapDirector struct {
	pool      *ldaputil.ConnectionPool
	baseDN    string
	filter    string
	attr      string
	resultFmt string
}

func NewLDAPDirector(uri, bindDN, bindPW, baseDN, filter, attr, resultFmt string) (Director, error) {
	if !strings.Contains(resultFmt, "%s") {
		return nil, errors.New("result_fmt does not contain the '%s' token")
	}
	if baseDN == "" {
		return nil, errors.New("LDAP base DN is unset")
	}
	if filter == "" {
		return nil, errors.New("LDAP filter is unset")
	}
	if attr == "" {
		return nil, errors.New("LDAP query attribute is unset")
	}

	pool, err := ldaputil.NewConnectionPool(uri, bindDN, bindPW, poolSize)
	if err != nil {
		return nil, err
	}
	return &ldapDirector{
		pool:      pool,
		baseDN:    baseDN,
		filter:    filter,
		attr:      attr,
		resultFmt: resultFmt,
	}, nil
}

func (d *ldapDirector) Lookup(ctx context.Context, email string) (string, error) {
	result, err := d.pool.Search(ctx, ldap.NewSearchRequest(
		d.baseDN,
		ldap.ScopeWholeSubtree,
		1, 0, 0, false,
		d.filter,
		[]string{d.attr},
		nil,
	))
	if err != nil {
		return "", err
	}
	if len(result.Entries) == 0 {
		return "", nil
	}

	return strings.Replace(d.resultFmt, "%s", result.Entries[0].GetAttributeValue(d.attr), -1), nil
}
